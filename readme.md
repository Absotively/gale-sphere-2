This was the first version of my sim for learning/practicing the Gale Sphere 2 mechanic from The Voidcast Dais (Extreme) in Final Fantasy XIV. I recommend using the [newer version](https://jenpollock.ca/ffxiv/sims/gale-sphere-2-v2/) [(source)](https://gitlab.com/Absotively/gale-sphere-2-v2) instead.

This is a [microStudio](https://microstudio.dev/) project.

[Play/use this here](https://jenpollock.ca/ffxiv/sims/gale-sphere-2/).