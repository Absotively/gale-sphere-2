let entities = [];
let ticks = 0;
let reset_avail_time = 0;
let rngChoices = {};
let started = false;
let ended = false;
let menu = null;

const yalm = 4;

function init() {
  entities = [];
  ticks = 0;
  reset_avail_time = 0;
  started = false;
  ended = false;
  menu = new Menu(menu);
  rollRNG();
  setUpDutySupport();
  setUpMechanics();
}

function update() {
  if (reset_avail_time != 0 && ticks > reset_avail_time) {
    if (keyboard.press.SPACE || keyboard.press.ENTER || gamepad.press.A) {
      init();
      party[menu.selected].player_controlled = true;
      started = true;
    } else if (keyboard.press.ESCAPE || gamepad.press.MENU) {
      init();
    }
  }
  if (started) {
    ticks += 1;
    if (!ended) {
      for (let e of entities) {
        e.update();
      }
    }
  } else {
    menu.update();
  }
}

function draw() {
  screen.setFont("BitCell");
  screen.fillRect(0,0,screen.width, screen.height,"rgb(43,0,0)");
  if (started) {
    screen.fillRect(0,0,30 * yalm,30 * yalm,"rgb(105,23,23)");
    for (let e of entities) {
      if (e.layer == 0) {
        e.draw();
      }
    }
    for (let e of entities) {
      if (e.layer == 1) {
        e.draw();
      }
    }
    
    if (reset_avail_time != 0 && ticks > reset_avail_time) {
      screen.drawText("Press SPACE or ENTER or gamepad A to restart", 0, -17 * yalm, 2 * yalm, "#fff");
      screen.drawText("Press ESC or the gamepad menu button to change settings", 0, -20 * yalm, 2 * yalm, "#fff");
    }
  } else {
    menu.draw();
  }
}

function endRun() {
  if (reset_avail_time == 0) {
    reset_avail_time = ticks + 60;
    ended = true;
  }
}

function rollRNG() {
  rngChoices.orb_origins = [];
  rngChoices.orb_lanesets = [];
  if (Math.random() < 0.5) {
    rngChoices.orb_origins.push("N");
    rngChoices.orb_origins.push("S");
  } else {
    rngChoices.orb_origins.push("S");
    rngChoices.orb_origins.push("N");
  }
  
  // the possible arrangements of safe lanes for the first two sets
  // of orbs are: W, middle; E, middle; middle, W; middle, E
  let dieRoll = Math.random();
  if (dieRoll < 0.25) {
    rngChoices.orb_lanesets.push([3,4,5,6]);
    rngChoices.orb_lanesets.push([1,2,5,6]);
  } else if (dieRoll < 0.5) {
    rngChoices.orb_lanesets.push([1,2,3,4]);
    rngChoices.orb_lanesets.push([1,2,5,6]);
  } else if (dieRoll < 0.75) {
    rngChoices.orb_lanesets.push([1,2,5,6]);
    rngChoices.orb_lanesets.push([3,4,5,6]);
  } else {
    rngChoices.orb_lanesets.push([1,2,5,6]);
    rngChoices.orb_lanesets.push([1,2,3,4]);
  }
  
  if (Math.random() < 0.5) {
    rngChoices.orb_origins.push("E");
    rngChoices.orb_origins.push("W");
  } else {
    rngChoices.orb_origins.push("W");
    rngChoices.orb_origins.push("E");
  }
  
  // safe lanes for the E-W orbs are either N, S or S, N
  if (Math.random() < 0.5) {
    rngChoices.orb_lanesets.push([1,2,3,4]);
    rngChoices.orb_lanesets.push([3,4,5,6]);
  } else {
    rngChoices.orb_lanesets.push([3,4,5,6]);
    rngChoices.orb_lanesets.push([1,2,3,4]);
  }
  
  if (Math.random() < 0.5) {
    rngChoices.iceSpikesFaceNS = true;
  } else {
    rngChoices.iceSpikesFaceNS = false;
  }
  
  if (Math.random() < 0.5) {
    rngChoices.iceSpikesHitNWSE = true;
  } else {
    rngChoices.iceSpikesHitNWSE = false;
  }
  
  if (Math.random() < 0.5) {
    rngChoices.stacksFirst = true;
  } else {
    rngChoices.stacksFirst = false;
  }
  
  if (Math.random() < 0.5) {
    rngChoices.enumsOnDPS = true;
  } else {
    rngChoices.enumsOnDPS = false;
  }
  
  print(JSON.stringify(rngChoices));
}

// all times are in ticks, there are 60 ticks per second
const next_clone_delay = 1*60;
const next_orbs_hit_delay = 4*60;

const orbs_warnings_duration = 1*60;
const hit_effect_duration = 0.75*60;
const clone_travel_time = 2*60;

const speech_appears_time = 60;
const castbar_start_time = 2*60;
const speech_disappears_time = 4.5*60;
const castbar_end_time = 5*60;
const first_clone_time = 7*60;
const orbs_appear_time = 12.5*60;
const first_orbs_hit_time = 18.28*60;
const ice_spikes_appear_time = first_orbs_hit_time - 30;
const ice_spikes_hit_time = first_orbs_hit_time + next_orbs_hit_delay;
const first_stacks_appear_time = 12*60;
const first_stacks_hit_time = first_orbs_hit_time;
const second_stacks_appear_time = 24*60;
const second_stacks_hit_time = first_orbs_hit_time + 3*next_orbs_hit_delay;
const success_time = second_stacks_hit_time;

function setUpMechanics() {
  entities.push(new Cast());
  
  if (rngChoices.stacksFirst) {
    entities.push(new LPStacks(first_stacks_appear_time, first_stacks_hit_time));
    entities.push(new Enums(second_stacks_appear_time, second_stacks_hit_time, rngChoices.enumsOnDPS));
  } else {
    entities.push(new Enums(first_stacks_appear_time, first_stacks_hit_time, rngChoices.enumsOnDPS));
    entities.push(new LPStacks(second_stacks_appear_time, second_stacks_hit_time));
  }
  
  let clone_time = first_clone_time;
  let hit_time = first_orbs_hit_time;
  for (let i = 0; i < 4; i++) {
    entities.push(new OrbSet(rngChoices.orb_origins[i], rngChoices.orb_lanesets[i],clone_time,hit_time));
    clone_time += next_clone_delay;
    hit_time += next_orbs_hit_delay;
  }
  
  entities.push(new IceSpikes(ice_spikes_appear_time, ice_spikes_hit_time));
  
  entities.push(new Success(success_time));
  
  console.log(entities);
}

let party = {};

function setUpDutySupport() {
  party.T1 = new PartyMember("T1","rgb(0,66,197)", 1, false, false);
  party.H1 = new PartyMember("H1","rgb(44,197,44)", 1, false, true);
  party.M1 = new PartyMember("M1","#f00", 1, true, false);
  party.R1 = new PartyMember("R1","#f00", 1, true, true);
  party.T2 = new PartyMember("T2","rgb(0,66,197)", 2, false, false);
  party.H2 = new PartyMember("H2","rgb(44,197,44)", 2, false, true);
  party.M2 = new PartyMember("M2","#f00", 2, true, false);
  party.R2 = new PartyMember("R2","#f00", 2, true, true);
  
  for (let p_name of Object.keys(party)) {
    let p = party[p_name];
    
    if (p.label == "T1") {
      p.set_start_position((-0.5 + Math.random()) * yalm, (4 + 2*Math.random()) * yalm);
    } else {
      p.set_start_position((-7 + 14*Math.random()) * yalm, (-8 + 4*Math.random()) * yalm);
    }
    
    let target_x = p.x;
    let target_y = p.y;
    
    // prepositioning for first stacks/enums
    if (p.lp == 1 && p.y < 2 * yalm) {
      target_y = (4 + 2*Math.random()) * yalm;
    } else if (p.lp == 2 && p.y > -2 * yalm) {
      target_y = (-4 - 2*Math.random()) * yalm;
    }
    // prepositioning for orbs - want to be not too far e/w
    if (p.x > 5 * yalm) {
      target_x = (3 + 2*Math.random()) * yalm;
    } else if (p.x < -5 * yalm) {
      target_x = (-3 - 2*Math.random()) * yalm;
    }
    p.step(new Step(castbar_start_time + thinking_delay, target_x, target_y));
    
    // prepositioning is adequate for stacks, need to move some for enums
    if (!rngChoices.stacksFirst) {
      let sign = p.lp == 1 ? 1 : -1;
      if (p.is_ranged) {
        target_y = (7.5 + 0.5*Math.random()) * yalm * sign;
      } else {
        target_y = Math.min(Math.abs(target_y), ((2.5 + 0.5*Math.random()) * yalm)) * sign;
      }
    }
    p.step(new Step(first_stacks_appear_time + thinking_delay, target_x, target_y));
    
    if (!rngChoices.orb_lanesets[0].includes(1)) {
      target_x = -6 * yalm;
    } else if (!rngChoices.orb_lanesets[0].includes(3)) {
      if (!rngChoices.orb_lanesets[1].includes(1)) {
        target_x = -4 * yalm;
      } else {
        target_x = 4 * yalm;
      }
    } else {
      target_x = 6 * yalm;
    }
    p.step(new Step(orbs_appear_time + thinking_delay, target_x, target_y));
    
    // includes some prepositioning on y-axis for orbs 3
    if (!rngChoices.orb_lanesets[1].includes(1)) {
      if (rngChoices.iceSpikesHitNWSE) {
        if (!rngChoices.orb_lanesets[2].includes(1)) {
          target_y = -1.5 * yalm;
        } else {
          target_y = -6.5 * yalm;
        }
      } else {
        if (!rngChoices.orb_lanesets[2].includes(1)) {
          target_y = 6.5 * yalm;
        } else {
          target_y = 1.5 * yalm;
        }
      }
      target_x = -6 * yalm;
    } else if (!rngChoices.orb_lanesets[1].includes(3)) {
      if (!rngChoices.orb_lanesets[2].includes(1)) {
        target_y = (6.5 * yalm * 3 + target_y) / 4;
      } else {
        target_y = (-6.5 * yalm * 3 + target_y) / 4;
      }
      if (rngChoices.iceSpikesHitNWSE) {
        target_x = target_y > 0 ? 3 * yalm : -3 * yalm;
      } else {
        target_x = target_y > 0 ? -3 * yalm : 3 * yalm;
      }
    } else {
      if (rngChoices.iceSpikesHitNWSE) {
        if (!rngChoices.orb_lanesets[2].includes(1)) {
          target_y = 6.5 * yalm;
        } else {
          target_y = 1.5 * yalm;
        }
      } else {
        if (!rngChoices.orb_lanesets[2].includes(1)) {
          target_y = -1.5 * yalm;
        } else {
          target_y = -6.5 * yalm;
        }
      }
      target_x = 6 * yalm;
    }
    p.step(new Step(first_orbs_hit_time + reaction_delay, target_x, target_y));
    
    if (!rngChoices.orb_lanesets[2].includes(1)) {
      target_y = 6 * yalm;
    } else {
      target_y = -6 * yalm;
    }
    // stack/enum prepositioning - do more movement before orbs 3 to use the
    // time gained by prepositioning for orbs 3
    let final_target_x = 0;
    let sign = p.lp == 1 ? -1 : 1;
    if (!rngChoices.stacksFirst) {
      final_target_x = (4 + Math.random()) * yalm * sign;
    } else if (p.is_ranged) {
      final_target_x = (6 + 0.5*Math.random()) * yalm * sign;
    } else {
      final_target_x = (2 + 0.5*Math.random()) * yalm * sign;
    }
    target_x = (target_x + 4*final_target_x) / 5;
    p.step(new Step(first_orbs_hit_time + next_orbs_hit_delay + reaction_delay, target_x, target_y));
  
    if (!rngChoices.orb_lanesets[3].includes(1)) {
      target_y = 6 * yalm;
    } else {
      target_y = -6 * yalm;
    }
    target_x = final_target_x;
    p.step(new Step(first_orbs_hit_time + 2*next_orbs_hit_delay + reaction_delay, target_x, target_y));
    
    entities.push(p);
  }
}

class SelfDrawingEntity {
  constructor(layer) {
    this.layer = layer; // must be 0 or 1
  }
  update() {}
  draw() {}
}

class Cast extends SelfDrawingEntity {
  constructor() {
    super(0);
  }
  
  draw() {
    if (ticks >= speech_appears_time && ticks < speech_disappears_time) {
      screen.setDrawAnchor(0,-1);
      screen.drawText("A raging gale to rend you asunder!",0,17*yalm,3*yalm,"#fff");
    }
    if (ticks >= castbar_start_time && ticks < castbar_end_time) {
      screen.setDrawAnchor(-1,-1);
      screen.drawText("Gale sphere", -15*yalm, -18*yalm, 2.5*yalm, "#fff");
      screen.fillRect(-15*yalm, -19.5*yalm, 30 * yalm * (ticks - castbar_start_time) / (castbar_end_time - castbar_start_time), 1*yalm,"rgb(255,189,57)");
      screen.drawRect(-15*yalm, -19.5*yalm, 30*yalm, 1*yalm, "#fff");
    }
    screen.setDrawAnchor(0,0);
  }
}

const orb_size = 5 * yalm;
const orb_color = "rgb(44,197,146)";
const orb_outline_color ="rgb(142,255,217)";
const orb_hit_effect_color = "rgb(12,105,74)";
const clone_size = 7 * yalm;
const max_clone_distance = 19 * yalm;
const clone_speed = max_clone_distance / clone_travel_time;

class OrbSet extends SelfDrawingEntity {
  constructor(origin, lanes, clone_time, hit_time) {
    super(0);
    this.origin = origin;
    this.lanes = lanes;
    this.clone_time = clone_time;
    this.hit_time = Math.round(hit_time);
    this.warnings_appear_time = hit_time - orbs_warnings_duration;
    this.effect_disappears_time = hit_time + hit_effect_duration;
  
    this.clone_x = 0;
    this.clone_y = 0;
    this.clone_x_speed = 0;
    this.clone_y_speed = 0;
    
    switch(origin) {
      case "N":
        this.clone_y_speed = clone_speed;
        break;
      case "S":
        this.clone_y_speed = -1 * clone_speed;
        break;
      case "W":
        this.clone_x_speed = -1 * clone_speed;
        break;
      case "E":
        this.clone_x_speed = clone_speed;
    }
  }
  
  update() {
    if (ticks >= this.clone_time && ticks < orbs_appear_time) {
      if (Math.abs(this.clone_x) > max_clone_distance) {
        this.clone_x_speed = 0;
      }
      if (Math.abs(this.clone_y) > max_clone_distance) {
        this.clone_y_speed = 0;
      }
      this.clone_x += this.clone_x_speed;
      this.clone_y += this.clone_y_speed;
    }
    if (ticks == this.hit_time) {
      for (let l of this.lanes) {
        switch (this.origin) {
          case "N":
          case "S":
            for (let e of entities) {
              if (e.ko && e.x >= (l-4)*orb_size && e.x <= (l-3)*orb_size) {
                e.ko();
              }
            }
            break;
          case "W":
          case "E":
            for (let e of entities) {
              if (e.ko && e.y <= (4-l)*orb_size && e.y >= (3-l)*orb_size) {
                e.ko();
              }
            }
            break;
        }
      }
    }
  }
  
  draw() {
    if (ticks >= this.clone_time && ticks < orbs_appear_time) {
      screen.fillRound(this.clone_x, this.clone_y, clone_size, clone_size,"rgb(124,87,197)");
    }
    if (ticks >= orbs_appear_time && ticks < this.effect_disappears_time) {
      // draw orbs
      for (let i of this.lanes) {
        let x = 0; let y= 0;
        switch (this.origin) {
          case "N":
            x = (i-3.5)*orb_size;
            y = 15 * yalm + 0.5 * orb_size;
            break;
          case "S":
            x = (i-3.5)*orb_size;
            y = -15 * yalm - 0.5 * orb_size;
            break;
          case "W":
            y = (3.5-i)*orb_size;
            x = -15 * yalm - 0.5 * orb_size;
            break;
          case "E":
            y = (3.5-i)*orb_size;
            x = 15 * yalm + 0.5 * orb_size;
            break;
        }
        screen.fillRound(x, y, orb_size, orb_size, orb_color);
        screen.drawRound(x, y, orb_size, orb_size, orb_outline_color);
      }
    }
    if (ticks >= this.warnings_appear_time && ticks < this.hit_time) {
      // draw indicators
      switch (this.origin) {
        case "N":
        case "S":
          for (let i of this.lanes) {
            screen.fillRect((i-3.5)*orb_size,0,orb_size,6*orb_size,"rgba(255,85,0,0.7)");
          }
          break;
        case "W":
        case "E":
          for (let i of this.lanes) {
            screen.fillRect(0,(3.5-i)*orb_size,6*orb_size,orb_size,"rgba(255,85,0,0.7)");
          }
          break;
      }
    }
    if (ticks >= this.hit_time && ticks < this.effect_disappears_time) {
      switch (this.origin) {
        case "N":
        case "S":
          for (let i of this.lanes) {
            screen.fillRect((i-3.5)*orb_size,0,orb_size,6*orb_size,"rgb(12,105,74)");
          }
          break;
        case "W":
        case "E":
          for (let i of this.lanes) {
            screen.fillRect(0,(3.5-i)*orb_size,6*orb_size,orb_size,"rgb(12,105,74)");
          }
          break;
      }
    }
  }
}

const ice_spikes_color ="rgb(198,255,255)";
const ice_spikes_effect_color = "rgba(198,255,255,0.7)";

class IceSpikes extends SelfDrawingEntity {
  constructor(appear_time, hit_time) {
    super(0);
    this.appear_time = appear_time
    this.hit_time = Math.round(hit_time);
  }
  
  update() {
    if (ticks == this.hit_time) {
      for (let e of entities) {
        if (e.ko) {
          if (rngChoices.iceSpikesHitNWSE) {
            if ((e.x <= 0 && e.y >= 0) || (e.x >= 0 && e.y <= 0)) {
              e.ko();
            }
          } else {
            if ((e.x <= 0 && e.y <= 0) || (e.x >= 0 && e.y >= 0)) {
              e.ko();
            }
          }
        }
      }
    }
  }
  
  draw() {
    if (ticks < this.appear_time) {
      return;
    } else if (ticks < this.hit_time) {
      if (rngChoices.iceSpikesFaceNS) {
        screen.drawLine(-15 * yalm, 0, 15* yalm, 0, ice_spikes_color);
        let spikeTips = rngChoices.iceSpikesHitNWSE ? 1.5 * yalm : -1.5 * yalm;
        for (let i = 0; i < 6; i++) {
          screen.drawPolyline(
            (2.3 + i*2.5) * yalm, 0,
            (2.5 + i*2.5) * yalm, -1 * spikeTips,
            (2.7 + i*2.5) * yalm, 0
          );
          screen.drawPolyline(
            (-2.3 - i*2.5) * yalm, 0,
            (-2.5 - i*2.5) * yalm, spikeTips,
            (-2.7 - i*2.5) * yalm, 0
          );
        }
      } else {
        screen.drawLine(0, -15 * yalm, 0, 15* yalm, ice_spikes_color);
        let spikeTips = rngChoices.iceSpikesHitNWSE ? 1.5 * yalm : -1.5 * yalm;
        for (let i = 0; i < 6; i++) {
          screen.drawPolyline(
            0, (2.3 + i*2.5) * yalm,
            -1 * spikeTips, (2.5 + i*2.5) * yalm,
            0, (2.7 + i*2.5) * yalm,
          );
          screen.drawPolyline(
            0, (-2.3 - i*2.5) * yalm,
            spikeTips, (-2.5 - i*2.5) * yalm,
            0, (-2.7 - i*2.5) * yalm
          );
        }
      }
    } else if (ticks < this.hit_time + hit_effect_duration) {
      if (rngChoices.iceSpikesHitNWSE) {
        screen.fillRect(-7.5 * yalm, 7.5 * yalm, 15 * yalm, 15 * yalm, ice_spikes_effect_color);
        screen.fillRect(7.5 * yalm, -7.5 * yalm, 15 * yalm, 15 * yalm, ice_spikes_effect_color);
      } else {
        screen.fillRect(7.5 * yalm, 7.5 * yalm, 15 * yalm, 15 * yalm, ice_spikes_effect_color);
        screen.fillRect(-7.5 * yalm, -7.5 * yalm, 15 * yalm, 15 * yalm, ice_spikes_effect_color);
      }
    }
  }
}

class Stacks extends SelfDrawingEntity {
  constructor(targets, soak_count, radius, color, show_time, hit_time) {
    super(0);
    this.targets = targets;
    this.soak_count = soak_count;
    this.radius = radius;
    this.color = color;
    this.show_time = show_time;
    this.hit_time = Math.round(hit_time);
  }
  
  update() {
    if (ticks == this.hit_time) {
      let hit = [];
      let to_ko = [];
      for (let target of this.targets) {
        let x = target.x;
        let y = target.y;
        let in_stack = [];
        for (let e of entities) {
          if (e.ko && !e.KOd && Math.sqrt(Math.pow(e.x - x, 2) + Math.pow(e.y - y, 2)) < this.radius) {
            in_stack.push(e);
            if (hit.includes(e)) {
              to_ko.push(e);
            } else {
              hit.push(e);
            }
          }
        }
        if (in_stack.length < this.soak_count) {
          for (let p of in_stack) {
            to_ko.push(p);
          }
        }
      }
      for (let p of to_ko) {
        p.ko();
      }
    }
  }
  
  draw() {
    if (ticks < this.show_time) {
      return;
    } else if (ticks < this.hit_time) {
      for (const target of this.targets) {
        screen.drawRound(target.x, target.y, 2 * this.radius, 2 * this.radius, this.color);
      }
    } else if (ticks < this.hit_time + hit_effect_duration) {
      for (const target of this.targets) {
        screen.fillRound(target.x, target.y, 2 * this.radius, 2 * this.radius,"rgba(5,5,43,0.7)");
      }
    }
  }
}

class LPStacks extends Stacks {
  constructor(show_time, hit_time) {
    super([party.H1, party.H2], 4, 6*yalm,"rgb(255,255,28)", show_time, hit_time);
  }
}

class Enums extends Stacks {
  constructor(show_time, hit_time, target_dps) {
    super(
      target_dps ? [party.M1, party.R1, party.M2, party.R2] : [party.T1, party.H1, party.T2, party.H2],
      2,
      3*yalm,
      "#fff",
      show_time,
      hit_time
    );
  }
}

class Success extends SelfDrawingEntity {
  constructor(time) {
    super(1);
    this.time = Math.round(time);
    this.show = false;
  }

  update() {
    if (ticks == this.time) {
      endRun();
      this.show = true;
      for (const e of entities) {
        if (e.KOd) {
          this.show = false;
          break;
        }
      }
    }
  }

  draw() {
    if (this.show) {
      screen.setLineWidth(3);
      screen.drawTextOutline("Success!", 0, 0, 5*yalm, "#000");
      screen.drawText("Success!", 0, 0, 5*yalm, "#fff");
      screen.setLineWidth(1);
    }
  }
}

const party_member_size = 2.5 * yalm;
const party_member_text_size = 0.7 * party_member_size;
const ko_sprite_size = 1.5 * party_member_size;

// https://www.youtube.com/watch?v=hZRQxaDK98o
const player_speed = 3.58 * yalm / 60;

const thinking_delay = 90;  // delay before duty support moves after tell shown
const reaction_delay = 20;  // delay before duty support moves after hit, when tell shown earlier

class Step {
  constructor(time, x, y) {
    this.time = time;
    this.x = x;
    this.y = y;
  }
}

class PartyMember extends SelfDrawingEntity {
  constructor(label, color, lp, is_dps, is_ranged) {
    super(1);
    this.label = label;
    this.color = color;
    this.KOd = false;
    this.lp = lp;
    this.is_dps = is_dps;
    this.is_ranged = is_ranged;
    this.movement_plan = [];
    this.player_controlled = false;
  }
  
  set_start_position(x, y) {
    this.x = x;
    this.y = y;
    this.movement_plan[0] = new Step(-1,x,y);
  }
  
  step(s) {
    this.movement_plan.push(s);
  }
  
  ko() {
    this.KOd = true;
    endRun();
  }
  
  update() {
    if (this.KOd) {
      return;
    }
    if (this.player_controlled) {
      let moving = false;
      let angle = 0;
      if (keyboard.UP || keyboard.W) {
        moving = true;
        if (keyboard.RIGHT || keyboard.D) {
          angle = 0.25 * Math.PI;
        } else if (keyboard.LEFT || keyboard.A) {
          angle = 0.75 * Math.PI;
        } else {
          angle = 0.5 * Math.PI;
        }
      } else if (keyboard.DOWN || keyboard.S) {
        moving = true;
        if (keyboard.RIGHT || keyboard.D) {
          angle = -0.25 * Math.PI;
        } else if (keyboard.LEFT || keyboard.A) {
          angle = -0.75 * Math.PI;
        } else {
          angle = -0.5 * Math.PI;
        }
      } else if (keyboard.RIGHT || keyboard.D) {
        moving = true;
        angle = 0;
      } else if (keyboard.LEFT || keyboard.A) {
        moving = true;
        angle = Math.PI;
      } else if (gamepad.LEFT_STICK_AMOUNT > 0.5) {
        moving = true;
        angle = Math.PI * 2 * gamepad.LEFT_STICK_ANGLE / 360;
      }
      if (moving) {
        this.x += player_speed * Math.cos(angle);
        this.y += player_speed * Math.sin(angle);
      }
    } else {
      let i = 0;
      while (i+1 < this.movement_plan.length && this.movement_plan[i+1].time < ticks) {
        i++;
      }
      let target_x = this.movement_plan[i].x;
      let target_y = this.movement_plan[i].y;
      if (this.x == target_x && this.y == target_y) {
        return;
      }
      let x_distance = target_x - this.x;
      let y_distance = target_y - this.y;
      if (Math.sqrt(x_distance * x_distance + y_distance * y_distance) < player_speed) {
        this.x = target_x;
        this.y = target_y;
      } else {
        let angle = Math.atan((target_y - this.y) / (target_x - this.x));
        if (target_x < this.x) {
          angle += Math.PI;
        }
        this.x += player_speed * Math.cos(angle);
        this.y += player_speed * Math.sin(angle);
      }
    }
  }
  
  draw() {
    screen.fillRound(this.x, this.y, party_member_size, party_member_size, this.color);
    screen.setLineWidth(0.5);
    screen.drawRound(this.x, this.y, party_member_size, party_member_size, this.player_controlled ? "#fff" : "#000");
    screen.setLineWidth(1);
    screen.drawText(this.label, this.x, this.y, party_member_text_size, "#fff");
    if (this.KOd) {
      screen.drawSprite("kod", this.x, this.y, ko_sprite_size, ko_sprite_size);
    }
  }
}
  
const menu_column_size = 16 * yalm;
const menu_row_size = 8 * yalm;
const menu_text_size = 4 * yalm;
const menu_button_padding = 1 * yalm;

const menu_buttons = {
  "T1": {"name": "T1", "row": 1, "column": 1, "left": "R1", "right": "H1", "up": "Start", "down": "T2", "radio": true},
  "H1": {"name": "H1", "row": 1, "column": 2, "left": "T1", "right": "M1", "up": "Start", "down": "H2", "radio": true},
  "M1": {"name": "M1", "row": 1, "column": 3, "left": "H1", "right": "R1", "up": "Start", "down": "M2", "radio": true},
  "R1": {"name": "R1", "row": 1, "column": 4, "left": "M1", "right": "T1", "up": "Start", "down": "R2", "radio": true},
  "T2": {"name": "T2", "row": 2, "column": 1, "left": "R2", "right": "H2", "up": "T1", "down": "None", "radio": true},
  "H2": {"name": "H2", "row": 2, "column": 2, "left": "T2", "right": "M2", "up": "H1", "down": "None", "radio": true},
  "M2": {"name": "M2", "row": 2, "column": 3, "left": "H2", "right": "R2", "up": "M1", "down": "None", "radio": true},
  "R2": {"name": "R2", "row": 2, "column": 4, "left": "M2", "right": "T2", "up": "R1", "down": "None", "radio": true},
  "None": {"name": "None", "row": 3, "column": 1, "left": "None", "right": "None", "up": "T2", "down": "Start", "radio": true},
  "Start": {"name": "Start", "row": 5, "column": 2.5, "left": "Start", "right": "Start", "up": "None", "down": "T1", "radio": false}
};

class Menu extends SelfDrawingEntity {
  constructor(old_menu) {
    super(1);
    if (old_menu) {
      this.selected = old_menu.selected;
      this.focus = "Start";
    } else {
      this.selected = "T1";
      this.focus = "T1";
    }
  }
  
  update() {
    if (keyboard.press.SPACE || keyboard.press.ENTER || gamepad.press.A) {
      if (this.focus == "Start") {
        if (this.selected != "None") {
          party[this.selected].player_controlled = true;
        }
        started = true;
      } else {
        this.selected = this.focus;
      }
    }
    if (keyboard.press.UP || keyboard.press.W || gamepad.press.DPAD_UP) {
      this.focus = menu_buttons[this.focus]["up"];
    }
    if (keyboard.press.DOWN || keyboard.press.S || gamepad.press.DPAD_DOWN) {
      this.focus = menu_buttons[this.focus]["down"];
    }
    if (keyboard.press.LEFT || keyboard.press.A || gamepad.press.DPAD_LEFT) {
      this.focus = menu_buttons[this.focus]["left"];
    }
    if (keyboard.press.RIGHT || keyboard.press.D || gamepad.press.DPAD_RIGHT) {
      this.focus = menu_buttons[this.focus]["right"];
    }
  }
  
  draw() {
    for (const button_name of Object.getOwnPropertyNames(menu_buttons)) {
      this.#drawButton(menu_buttons[button_name]);
    }
  }
  
  #drawButton(b) {
    let left = (-3 + b.column) * menu_column_size;
    let top = (3 - b.row)  * menu_row_size;
    if (b.radio) {
      screen.setDrawAnchor(-1, 1);
      if (this.selected == b.name) {
        screen.fillRound(left, top, menu_text_size, menu_text_size,"rgb(255,179,28)");
      }
      screen.drawRound(left, top, menu_text_size, menu_text_size, "#fff");
      screen.drawText(b.name, left + 1.5 * menu_text_size, top, menu_text_size, "#fff");
      if (this.focus == b.name) {
        screen.drawRect(
          left - menu_button_padding,
          top + menu_button_padding,
          menu_column_size - menu_button_padding,
          menu_text_size + 2 * menu_button_padding,
          "#fff"
        );
      }
    } else {
      screen.setDrawAnchor(0, 1);
      let centre = left + 0.5 * menu_column_size;
      screen.drawText(b.name, centre, top, menu_text_size, "#fff");
      if (this.focus == b.name) {
        screen.drawRect(centre, top + menu_button_padding, 2 * menu_column_size, menu_text_size + 2 * menu_button_padding, "#fff");
      }
    }
    screen.setDrawAnchor(0, 0);
  }
}